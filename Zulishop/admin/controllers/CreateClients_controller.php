<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Rols_controller
 *
 * @author pabhoz
 */
class CreateClients_controller extends BController{

    function __construct() {
        parent::__construct();
    }

    public function index()
    {   $this->view->clients = Clients_bl::getAll();
        $this->view->render($this,"index");
    }
    
    public function create(){
        $r = Clients_bl::create($_POST);
        print(json_encode($r));
    }
    

}
