</br>
<div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Creación de Productos</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="formularioCrearProducto" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="nameInput" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nameInput" placeholder="Name" name="name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="priceInput" class="col-sm-2 control-label">Price</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="priceInput" placeholder="Price" name="price">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="quantityInput" class="col-sm-2 control-label">Quantity</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="quantityInput" placeholder="Quantity" name="quantity">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="brandInput" class="col-sm-2 control-label"> ID Brand</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="brandInput" placeholder="ID Brand" name="brand">
                  </div>
                </div>
                 <div class="form-group">
                  <label for="disscountInput" class="col-sm-2 control-label">Disscount</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="disscountInput" placeholder="Disscount" name="disscount">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Añadir producto</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <?php $this->asyncCreation("#formularioCrearProducto","CreateProducts/create","Product"); ?>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Ramas disponibles</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tbody><tr>
                  <th>ID</th>
                  <th>Brand</th>
                </tr>
                <?php foreach ($this->brands as $brands) : ?>
                <tr>
                  <td><?php print $brands->getId(); ?></td>
                  <td><?php print $brands->getName(); ?></td>
                 
                </tr>
                <?php endforeach; ?>
              </tbody></table>
            </div>
    
            <!-- /.box-body -->
          </div>