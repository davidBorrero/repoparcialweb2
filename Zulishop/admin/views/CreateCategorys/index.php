</br>
<div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Creación de Categorias</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="formularioCrearCategoria" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="nameInput" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nameInput" placeholder="Name" name="name">
                  </div>
                </div>
                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Añadir categoria</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <?php $this->asyncCreation("#formularioCrearCategoria","CreateCategorys/create","Category"); ?>
