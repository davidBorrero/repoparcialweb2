</br>
<div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Creación de Roles</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="formularioCool" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="rolInput" class="col-sm-2 control-label"> ID Rol</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="rolInput" placeholder="Rol" name="rol">
                  </div>
                </div>
                <div class="form-group">
                  <label for="startInput" class="col-sm-2 control-label">Start</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="startInput" placeholder="Start" name="start">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crear</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <?php $this->asyncCreation("#formularioCool","Rols/create","Rol"); ?>