</br>
<div class="col-md-12">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Creación de Marcas</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="formularioCrearMarca" class="form-horizontal">
              <div class="box-body">
                <div class="form-group">
                  <label for="nameInput" class="col-sm-2 control-label">Name</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="nameInput" placeholder="Name" name="name">
                  </div>
                </div>
                <div class="form-group">
                  <label for="logoInput" class="col-sm-2 control-label">Logo</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="logoInput" placeholder="Logo" name="logo">
                  </div>
                </div>
                 
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Añadir marca</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
          <?php $this->asyncCreation("#formularioCrearMarca","CreateBrands/create","Brand"); ?>
