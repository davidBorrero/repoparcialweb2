-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema zulishopP
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `zulishopP` ;

-- -----------------------------------------------------
-- Schema zulishopP
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zulishopP` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `zulishopP` ;

-- -----------------------------------------------------
-- Table `zulishopP`.`Client`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Client` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Client` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE UNIQUE INDEX `email_UNIQUE` ON `zulishopP`.`Client` (`email` ASC);

CREATE UNIQUE INDEX `username_UNIQUE` ON `zulishopP`.`Client` (`username` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Rol`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Rol` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Rol` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rol` VARCHAR(45) NOT NULL,
  `start` VARCHAR(80) NOT NULL DEFAULT 'Blank/',
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zulishopP`.`User`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`User` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`User` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `rol` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_User_Rol`
    FOREIGN KEY (`rol`)
    REFERENCES `zulishopP`.`Rol` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `username_UNIQUE` ON `zulishopP`.`User` (`username` ASC);

CREATE UNIQUE INDEX `email_UNIQUE` ON `zulishopP`.`User` (`email` ASC);

CREATE INDEX `fk_User_Rol_idx` ON `zulishopP`.`User` (`rol` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Category` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `parent` INT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Category_Category1`
    FOREIGN KEY (`parent`)
    REFERENCES `zulishopP`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `zulishopP`.`Category` (`name` ASC);

CREATE INDEX `fk_Category_Category1_idx` ON `zulishopP`.`Category` (`parent` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Subcategory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Subcategory` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Subcategory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `category` INT NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Subcategory_Category1`
    FOREIGN KEY (`category`)
    REFERENCES `zulishopP`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE UNIQUE INDEX `name_UNIQUE` ON `zulishopP`.`Subcategory` (`name` ASC);

CREATE INDEX `fk_Subcategory_Category1_idx` ON `zulishopP`.`Subcategory` (`category` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Brand`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Brand` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Brand` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `logo` VARCHAR(128) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zulishopP`.`Product`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Product` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Product` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `price` DECIMAL(15,2) NOT NULL DEFAULT 0,
  `quantity` VARCHAR(45) NOT NULL DEFAULT 0,
  `brand` INT NOT NULL,
  `disscount` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Product_Brand1`
    FOREIGN KEY (`brand`)
    REFERENCES `zulishopP`.`Brand` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_Brand1_idx` ON `zulishopP`.`Product` (`brand` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Provider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Provider` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Provider` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `tel` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `zulishopP`.`Product_x_Provider`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Product_x_Provider` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Product_x_Provider` (
  `Product_id` INT NOT NULL,
  `Provider_id` INT NOT NULL,
  PRIMARY KEY (`Product_id`, `Provider_id`),
  CONSTRAINT `fk_Product_has_Provider_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishopP`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_Provider_Provider1`
    FOREIGN KEY (`Provider_id`)
    REFERENCES `zulishopP`.`Provider` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_has_Provider_Provider1_idx` ON `zulishopP`.`Product_x_Provider` (`Provider_id` ASC);

CREATE INDEX `fk_Product_has_Provider_Product1_idx` ON `zulishopP`.`Product_x_Provider` (`Product_id` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Product_x_Category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Product_x_Category` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Product_x_Category` (
  `Product_id` INT NOT NULL,
  `Category_id` INT NOT NULL,
  PRIMARY KEY (`Product_id`, `Category_id`),
  CONSTRAINT `fk_Product_has_Category_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishopP`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_Category_Category1`
    FOREIGN KEY (`Category_id`)
    REFERENCES `zulishopP`.`Category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_has_Category_Category1_idx` ON `zulishopP`.`Product_x_Category` (`Category_id` ASC);

CREATE INDEX `fk_Product_has_Category_Product1_idx` ON `zulishopP`.`Product_x_Category` (`Product_id` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`ShoppingCar`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`ShoppingCar` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`ShoppingCar` (
  `Client_id` INT NOT NULL,
  `Product_id` INT NOT NULL,
  `quantity` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`Client_id`, `Product_id`),
  CONSTRAINT `fk_Client_has_Product_Client1`
    FOREIGN KEY (`Client_id`)
    REFERENCES `zulishopP`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Client_has_Product_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishopP`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Client_has_Product_Product1_idx` ON `zulishopP`.`ShoppingCar` (`Product_id` ASC);

CREATE INDEX `fk_Client_has_Product_Client1_idx` ON `zulishopP`.`ShoppingCar` (`Client_id` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Receipt` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `date` TIMESTAMP NOT NULL,
  `client` INT NOT NULL,
  `total` DECIMAL(15,2) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Receipt_Client1`
    FOREIGN KEY (`client`)
    REFERENCES `zulishopP`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Receipt_Client1_idx` ON `zulishopP`.`Receipt` (`client` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Product_x_Receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Product_x_Receipt` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Product_x_Receipt` (
  `Product_id` INT NOT NULL,
  `Receipt_id` INT NOT NULL,
  `quantity` INT NOT NULL,
  `price` DECIMAL(15,2) NOT NULL COMMENT 'Unit price at time of sale',
  PRIMARY KEY (`Product_id`, `Receipt_id`),
  CONSTRAINT `fk_Product_has_Receipt_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishopP`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_Receipt_Receipt1`
    FOREIGN KEY (`Receipt_id`)
    REFERENCES `zulishopP`.`Receipt` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Product_has_Receipt_Receipt1_idx` ON `zulishopP`.`Product_x_Receipt` (`Receipt_id` ASC);

CREATE INDEX `fk_Product_has_Receipt_Product1_idx` ON `zulishopP`.`Product_x_Receipt` (`Product_id` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Menu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Menu` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Menu` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `rol` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_Menu_Rol1`
    FOREIGN KEY (`rol`)
    REFERENCES `zulishopP`.`Rol` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Menu_Rol1_idx` ON `zulishopP`.`Menu` (`rol` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`MenuItem`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`MenuItem` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`MenuItem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `url` VARCHAR(256) NOT NULL,
  `parent` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_MenuItem_MenuItem1`
    FOREIGN KEY (`parent`)
    REFERENCES `zulishopP`.`MenuItem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_MenuItem_MenuItem1_idx` ON `zulishopP`.`MenuItem` (`parent` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Menu_x_Item`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Menu_x_Item` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Menu_x_Item` (
  `Menu_id` INT NOT NULL,
  `MenuItem_id` INT NOT NULL,
  PRIMARY KEY (`Menu_id`, `MenuItem_id`),
  CONSTRAINT `fk_Menu_has_MenuItem_Menu1`
    FOREIGN KEY (`Menu_id`)
    REFERENCES `zulishopP`.`Menu` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Menu_has_MenuItem_MenuItem1`
    FOREIGN KEY (`MenuItem_id`)
    REFERENCES `zulishopP`.`MenuItem` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Menu_has_MenuItem_MenuItem1_idx` ON `zulishopP`.`Menu_x_Item` (`MenuItem_id` ASC);

CREATE INDEX `fk_Menu_has_MenuItem_Menu1_idx` ON `zulishopP`.`Menu_x_Item` (`Menu_id` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Comentario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Comentario` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Comentario` (
  `text` VARCHAR(45) NULL,
  `Product_id` INT NOT NULL,
  `Client_id` INT NOT NULL,
  CONSTRAINT `fk_Comentario_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishopP`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Comentario_Client1`
    FOREIGN KEY (`Client_id`)
    REFERENCES `zulishopP`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Comentario_Product1_idx` ON `zulishopP`.`Comentario` (`Product_id` ASC);

CREATE INDEX `fk_Comentario_Client1_idx` ON `zulishopP`.`Comentario` (`Client_id` ASC);


-- -----------------------------------------------------
-- Table `zulishopP`.`Calificacion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zulishopP`.`Calificacion` ;

CREATE TABLE IF NOT EXISTS `zulishopP`.`Calificacion` (
  `Calificacion` FLOAT NULL,
  `Product_id` INT NOT NULL,
  `Client_id` INT NOT NULL,
  CONSTRAINT `fk_Calificacion_Product1`
    FOREIGN KEY (`Product_id`)
    REFERENCES `zulishopP`.`Product` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Calificacion_Client1`
    FOREIGN KEY (`Client_id`)
    REFERENCES `zulishopP`.`Client` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_Calificacion_Product1_idx` ON `zulishopP`.`Calificacion` (`Product_id` ASC);

CREATE INDEX `fk_Calificacion_Client1_idx` ON `zulishopP`.`Calificacion` (`Client_id` ASC);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `zulishopP`.`Rol`
-- -----------------------------------------------------
START TRANSACTION;
USE `zulishopP`;
INSERT INTO `zulishopP`.`Rol` (`id`, `rol`, `start`) VALUES (1, 'SuperAdmin', 'Users/index');
INSERT INTO `zulishopP`.`Rol` (`id`, `rol`, `start`) VALUES (2, 'Admin', 'Users/index');
INSERT INTO `zulishopP`.`Rol` (`id`, `rol`, `start`) VALUES (3, 'SalesMan', 'Products/index');
INSERT INTO `zulishopP`.`Rol` (`id`, `rol`, `start`) VALUES (4, 'CellarMan', 'Blank/');
INSERT INTO `zulishopP`.`Rol` (`id`, `rol`, `start`) VALUES (5, 'Accountman', 'Blank/');

COMMIT;


-- -----------------------------------------------------
-- Data for table `zulishopP`.`User`
-- -----------------------------------------------------
START TRANSACTION;
USE `zulishopP`;
INSERT INTO `zulishopP`.`User` (`id`, `username`, `password`, `email`, `rol`) VALUES (1, 'admin', '1234', 'admin@admin.com', 1);
INSERT INTO `zulishopP`.`User` (`id`, `username`, `password`, `email`, `rol`) VALUES (2, 'test', '1234', 'test@test.com', 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `zulishopP`.`Menu`
-- -----------------------------------------------------
START TRANSACTION;
USE `zulishopP`;
INSERT INTO `zulishopP`.`Menu` (`id`, `name`, `rol`) VALUES (1, 'SuperMenu', 1);
INSERT INTO `zulishopP`.`Menu` (`id`, `name`, `rol`) VALUES (2, 'SalesMans', 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `zulishopP`.`MenuItem`
-- -----------------------------------------------------
START TRANSACTION;
USE `zulishopP`;
INSERT INTO `zulishopP`.`MenuItem` (`id`, `name`, `url`, `parent`) VALUES (1, 'Usuarios', 'Users', NULL);
INSERT INTO `zulishopP`.`MenuItem` (`id`, `name`, `url`, `parent`) VALUES (2, 'Productos', 'Products', NULL);
INSERT INTO `zulishopP`.`MenuItem` (`id`, `name`, `url`, `parent`) VALUES (3, 'Roles', 'Rols', NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `zulishopP`.`Menu_x_Item`
-- -----------------------------------------------------
START TRANSACTION;
USE `zulishopP`;
INSERT INTO `zulishopP`.`Menu_x_Item` (`Menu_id`, `MenuItem_id`) VALUES (1, 1);
INSERT INTO `zulishopP`.`Menu_x_Item` (`Menu_id`, `MenuItem_id`) VALUES (1, 2);
INSERT INTO `zulishopP`.`Menu_x_Item` (`Menu_id`, `MenuItem_id`) VALUES (2, 2);
INSERT INTO `zulishopP`.`Menu_x_Item` (`Menu_id`, `MenuItem_id`) VALUES (1, 3);

COMMIT;

