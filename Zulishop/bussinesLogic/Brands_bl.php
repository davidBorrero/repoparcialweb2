<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Brands_bl {
 
public static function getBrand($id){
     $brand = Brand::getById($id);
     if(isset($brand)){
     $brand->rolDetail = Brand::getById($brand->getName());
     return $brand;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
     $brands = Brand::getAll();
     foreach ( $brands as $n => $brand){
         $brands[$n] = self::getBrand($brand["id"]);
     }
     return $brands;
 }
    
 
 public static function create($data){
    return Brand::instanciate($data)->create();
 }
   
  

}
