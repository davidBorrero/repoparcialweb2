<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Example_bl
 *
 * @author Pabhoz
 */
class Rols_bl {
 
public static function getRol($id){
     $rol = Rol::getById($id);
     if(isset($rol)){
     $rol->rolDetail = Rol::getById($rol->getRol());
     return $rol;
     }else{
         return false;
     }
 }
 
 public static function getAll(){
     $rols = Rol::getAll();
     foreach ( $rols as $n => $rol){
         $rols[$n] = self::getRol($rol["id"]);
     }
     return $rols;
 }
    
 
 public static function create($data){
    return Rol::instanciate($data)->create();
 }

}
